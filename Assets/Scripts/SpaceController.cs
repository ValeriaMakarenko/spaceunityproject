﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpaceController : MonoBehaviour {

	float speed = 0.000000001f;

	float maxSpeed = -0.1f;

	void FixedUpdate () {
		if(-0.04f - speed > maxSpeed){
			transform.Translate(-0.04f - speed, 0, 0);
			speed += 0.000000001f;
		}
	}
}