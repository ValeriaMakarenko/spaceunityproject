﻿using UnityEngine;
using System.Collections;

public class FloorScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag != "Player" && other.tag != "asteroid"){
			GameObject.Destroy(other.gameObject);
		}
	}
}
