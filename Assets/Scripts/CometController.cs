﻿using UnityEngine;
using System.Collections;

public class CometController : MonoBehaviour {

	bool isFly = false;

	void Update() {
		if (isFly) transform.Translate(-0.3f, -0.1f, 0);
	}

	void OnTriggerEnter2D(Collider2D other){
    	if(other.tag == "Player"){
    		isFly = true;
		}
    }

    void OnCollisionEnter2D(Collision2D collision){
		if(collision.collider.tag == "Finish"){
			Destroy(gameObject);
		}
    }
}
