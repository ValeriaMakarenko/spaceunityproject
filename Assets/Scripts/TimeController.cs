﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeController : MonoBehaviour {

	public Text textLabel;

	void Start () {
	}
	
	void Update () {
		string minutesStr = Mathf.Round(Time.realtimeSinceStartup/60).ToString();
		string secondsStr = Mathf.Round(Time.realtimeSinceStartup%60).ToString();
		if (minutesStr.Length == 1){
			minutesStr = "0"+minutesStr;
		}
		if (secondsStr.Length == 1){
			secondsStr = "0"+secondsStr;
		}
		textLabel.text = minutesStr + ":" + secondsStr;
	}
}
