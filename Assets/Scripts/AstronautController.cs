﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class AstronautController : MonoBehaviour {
    Vector3 move;
    float lastYPosition;
    float speed = 1.0f;

	public GameObject fire;
	public GameObject gun;
	public GameObject bulletTrigger;
	public GameObject bullet;

	public Image gasProgress;
	public Image tankProgress;

	public List<GameObject> lives;

	public Text timeLabel;
	public Text gameOverLabel;

	private String gameTime;
	private float lastBulletTime;
	private Vector2 prev;

	void Start () {
		lastBulletTime = Time.time;
		gameOverLabel.gameObject.SetActive(false);
	}
	
	void FixedUpdate () {
		updateTime();
		reduceOxygen();

		bool isJetpackActive = Input.GetButton("Horizontal") || Input.GetButton("Vertical");
		bool isGunActive = Input.GetButton("Fire1");
		bool isForceActive = Input.GetButton("Fire2");
		
		fire.SetActive(isJetpackActive);
		gun.SetActive(isGunActive);

		if(isGunActive && isBulletCan()){
	    	GameObject currentBullet = (GameObject)Instantiate(bullet);
	    	currentBullet.transform.position = new Vector3(bulletTrigger.transform.position.x, bulletTrigger.transform.position.y, 0);
	    	gun.gameObject.GetComponent<AudioSource>().Play();
	    	lastBulletTime = Time.time;
	    }

	    if(isJetpackActive){
	    	move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
	    	if (isForceActive && gasProgress.fillAmount > 0) {
				transform.position += move * Time.deltaTime * 2 * speed;
				gasProgress.fillAmount -= 0.005f;
	    	} else {
				transform.position += move * Time.deltaTime * speed;
	    	}
	    	lastYPosition = transform.position.y;
	    } else {
	    	setDormancy();
	    }
	}

	private void updateTime(){
		string minutesStr = Mathf.Round(Time.realtimeSinceStartup/60).ToString();
		string secondsStr = Mathf.Round(Time.realtimeSinceStartup%60).ToString();
		if (minutesStr.Length == 1){
			minutesStr = "0"+minutesStr;
		}
		if (secondsStr.Length == 1){
			secondsStr = "0"+secondsStr;
		}
		gameTime = minutesStr + ":" + secondsStr;
		timeLabel.text = gameTime;
	}


	private void reduceOxygen(){
		if (tankProgress.fillAmount > 0){
			tankProgress.fillAmount -= 0.0002f;
		} else {
			removeAllLives();
		}
	}

	private bool isBulletCan() { 
		return Time.time - lastBulletTime > 1.0f;
	}

	private void setDormancy(){
    	transform.position = new Vector3(transform.position.x, lastYPosition + Mathf.Sin(Time.time * 1.5f)/3.0f, transform.position.z);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		switch(collision.collider.gameObject.tag){
			case "gas": {
				print("gas");
				gasProgress.fillAmount = 1f;
				Destroy(collision.collider.gameObject);
				break;
			}
			case "tank": {
				print("tank");
				tankProgress.fillAmount = 1f;
				Destroy(collision.collider.gameObject);
				break;
			}
			case "asteroid": {
				GetComponent<AudioSource>().Play();
				print("asteroid");
				GetComponent<Animator>().SetTrigger("OnCollision");
				removeLife();
				Destroy(collision.collider.gameObject);
				break;
			}
			case "comet": {
				GetComponent<AudioSource>().Play();
				print("comet");
				GetComponent<Animator>().SetTrigger("OnCollision");
				removeLife();
				Destroy(collision.collider.gameObject);
				break;
			}
			case "planet":{
				GetComponent<AudioSource>().Play();
				break;
			}
		}
    }

    void OnTriggerEnter2D(Collider2D other){
    	if (other.tag == "blackhole" || other.tag == "destroy"){
    		removeAllLives();
		}
    }

    void OnTriggerExit2D(Collider2D other){
    	if(other.tag == "planet"){
    		GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
    	}
    }

    private void removeLife(){
    	lives[lives.Count-1].SetActive(false);
    	lives.RemoveAt(lives.Count-1);
		if(lives.Count == 0){
			setGameOver();
		}
    }

    private void removeAllLives(){
    	for (int i = 0; i <= lives.Count-1; i++) {
    		print(lives.Count);
    		lives[i].SetActive(false);
    	}
    	setGameOver();
    }

    public void setGameOver(){
    	GetComponent<Animator>().SetTrigger("GameOver");
		gameOverLabel.text = "GAME OVER\nYour time: "+ gameTime;
   		gameOverLabel.gameObject.SetActive(true);
    }

    public void DestroyAstronaut(){
    	Destroy(gameObject);
    }

    public void LoadMenu(){
    	SceneManager.LoadScene("MainMenu");
    }
}