﻿using UnityEngine;
using System.Collections;

public class BlackholeController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){
    	if(other.tag == "Player"){
    		GetComponent<Animator>().SetTrigger("OnPlayer");
    	}
    }
}
