﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
    
	void Start () {
		Destroy(gameObject, 10);
	}
	
	void FixedUpdate () {
		transform.position = new Vector3(transform.position.x+0.04F, transform.position.y, transform.position.z);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		Destroy(gameObject);
	}
}