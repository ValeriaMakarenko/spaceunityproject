﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyerController : MonoBehaviour {
	public List<GameObject> planets;
	public GameObject gas;
	public GameObject tank;

	public LevelObject[] level1;
	public LevelObject[] level2;
	public LevelObject[] level3;
	public LevelObject[] level4;
	public LevelObject[] level5;
	public LevelObject[] level6;
	public LevelObject[] level7;
	public LevelObject[] level8;
	public LevelObject[] level9;
	public LevelObject[] level10;

	List<LevelObject[]> levels = new List<LevelObject[]>();

	System.Random random = new System.Random();

	void Start () {
		levels.Add(level1);
		levels.Add(level2);
		levels.Add(level3);
		levels.Add(level4);
		levels.Add(level5);
		levels.Add(level6);
		levels.Add(level7);
		levels.Add(level8);
		levels.Add(level9);
		levels.Add(level10);
		CreateLevel(GameObject.Find("space (2)"));
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "space"){
			other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x + 48, other.gameObject.transform.position.y, other.gameObject.transform.position.z);
			foreach (Transform child in other.transform) {
				if(child.tag != "background") {
					GameObject.Destroy(child.gameObject);
				}
			}
			CreateLevel(other.gameObject);
		} 
	}

	void CreateLevel(GameObject space){
		LevelObject[] level = levels[random.Next(levels.Count)];
		foreach (LevelObject g in level){
			GameObject thing = (GameObject)Instantiate(g.obj, new Vector3(g.xposition, g.yposition, 0), g.obj.transform.rotation);
			thing.transform.parent = space.transform;
		}
	}
}

	[System.Serializable]
	public struct LevelObject {
     	public GameObject obj;
     	public float xposition;
     	public float yposition;
 	}
