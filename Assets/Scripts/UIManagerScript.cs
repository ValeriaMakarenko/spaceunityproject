﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManagerScript : MonoBehaviour {

	public Image loading;

	public void Start() {
		loading.enabled = false;
	}

	public void StartGame() {
    	SceneManager.LoadScene("SpaceProject");
    	loading.enabled = true;
	}

	public void QuitGame() {
    	 Application.Quit();
	}
}
