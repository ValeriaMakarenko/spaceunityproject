﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AsteroidController : MonoBehaviour {

	bool isFly = false;

	void FixedUpdate () {
		if (isFly) transform.position = new Vector3(transform.position.x-0.06f, transform.position.y, transform.position.z);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.collider.tag == "bullet"){
			GetComponent<Animator>().SetTrigger("OnBullet");
		} else if(collision.collider.tag == "planet"){
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
    	if(other.tag == "Player"){
    		isFly = true;
		}
    }

	public void DestroyAsteroid(){
    	Destroy(gameObject);
    }
}